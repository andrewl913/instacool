//
//  LoginViewController.swift
//  Rumor
//
//  Created by Andrew Lincoln on 12/21/14.
//  Copyright (c) 2014 Andrew Lincoln. All rights reserved.
//

import Foundation
import UIKit


class LoginViewController : UIViewController {
  
  //Views
  @IBOutlet weak private var usernameTextBox: UITextField!
  @IBOutlet weak private var passwordTextBox: UITextField!
  @IBOutlet weak private var loginButton: UIButton!
  @IBOutlet weak private var facebookLoginButton: UIButton!
  @IBOutlet weak private var registerButton: UIButton!
  
  //Animation Timer and animation check
  
  private var animsDidOccur  = false
  
  //Facebook Permissions
  private let permissions = ["public_profile"]
  
  //Login Dependencies
  private var errorAlreadyOccured = false
  
  
  //******************* IBACTIONS ******************
  
  @IBAction func facebookLogin(sender: AnyObject) {
    
    //Login With Facebook
    CATransaction.begin();
    facebookLoginButton.layer.removeAllAnimations();
    CATransaction.commit();
    
    
    PFFacebookUtils.logInWithPermissions(permissions, {
      (user: PFUser!, error: NSError!) -> Void in
      if (user == nil) {
        println("The user cancelled the Facebook login.")
        if(error != nil) {
          println("There was an error logging in\(error.description)")
          
        }
        
      } else if (user.isNew) {
        
        user.username = self.usernameTextBox.text
        println("Show user around")
        //TODO: Push to AppNavigationController
        UIView.animateWithDuration(NSTimeInterval(0.5), animations: {
          
          
          self.facebookLoginButton.transform = CGAffineTransformMakeScale(0.001, 0.001);
          
          }, completion: { done in
            
            self.pushToAppNavigationController(self)
            
        })
        
        
      } else {
        println("Already Logged Into Facebook.. call from LoginViewController")
        
        UIView.animateWithDuration(NSTimeInterval(0.5), animations: {
          
          
          self.facebookLoginButton.transform = CGAffineTransformMakeScale(0.001, 0.001);
          
          }, completion: { done in
            
            self.pushToAppNavigationController(self)
            
        })
        
      }
    })
  }// end facebookLogin
  
  @IBAction func resetPassword(sender: AnyObject) {
    var isPasswordSent = false
    var userEmail : String! = ""
    var alertView : UIAlertController
    
    alertView = UIAlertController(title: "Forgot Password?", message: "Please enter your email address", preferredStyle: UIAlertControllerStyle.Alert);
    alertView.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler: nil))
    alertView.addTextFieldWithConfigurationHandler({
      (field: UITextField!) in
      
      field.placeholder = "Email"
      
      //add an observer to the textfield to check for real time updates.
      NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: field, queue: NSOperationQueue.mainQueue(), usingBlock: {
        (notification) in
        
        //assign the password empty if password was sent
        if(isPasswordSent) {
          userEmail = ""
        } else {
          userEmail = field.text
        }
        
        // Send the password request to Parse
        PFUser.requestPasswordResetForEmailInBackground(userEmail, block: {
          (success: Bool, error : NSError!)  in
          if(success == true) {
            isPasswordSent = true
            alertView.message = "Password sent to your email address"
          } else {
            alertView.message = "Sorry, that email address doesn't exist"
          }
          
        })
        //DEBUG:
        println("text changed!\(userEmail)")
        
      })
      
    })
    
    
    
  }
  
  
  //when focus on userName, clear background from any error colors that may
  //have occured from an unsuccessful log in attempt.
  @IBAction func userNameEditDidChange(sender: AnyObject) {
    usernameTextBox.backgroundColor = UIColor.whiteColor()
  }
  
  @IBAction func userNameEditDidEnd(sender: AnyObject) {
    usernameTextBox.backgroundColor = UIColor.whiteColor()
    
  }
  
  
  
  @IBAction func loginButton(sender: AnyObject) {
    
    PFUser.logInWithUsernameInBackground(usernameTextBox.text, password: passwordTextBox.text) {
      (user: PFUser!, error : NSError!) in
      if(user != nil) {
        
        self.pushToAppNavigationController(self)
        self.errorAlreadyOccured = false
        
      } else {
        
        if(!self.errorAlreadyOccured) {
          
          self.loginButton.transform = CGAffineTransformMakeScale(0, 0)
          self.errorAlreadyOccured = true
        }
        self.loginButton.setBackgroundImage(UIAssets.imageOfTryAgainButton, forState: UIControlState.Normal)
        self.loginButton.setBackgroundImage(UIAssets.imageOfTryAgainButtonPushed, forState: .Highlighted)
        self.loginButton.layer.zPosition = 1
        UIView.animateWithDuration(0.5, delay: 0.1, usingSpringWithDamping: 1.5, initialSpringVelocity: 1.1, options: nil, animations:{
          
          self.loginButton.transform = CGAffineTransformMakeScale(1, 1)
          
          
          }, completion: nil)
        self.usernameTextBox.text = ""
        self.usernameTextBox.backgroundColor = UIAssets.errorRed
        self.usernameTextBox.placeholder = "Invalid!"
        //DEBUG:
        println("Login has failed")
      }
      
    }
    //DEBUG:
    println("Fired")
  }
  
  
  //******************* PUBLIC METHODS  ******************
  
  
  
  class func pushToAppNavigationController(controller : UIViewController, segueIdentifier: String){
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let appNavigationController = storyboard.instantiateViewControllerWithIdentifier("AppNavigationController") as AppNavigationController
    let appNavSegue = UIStoryboardSegue(identifier: segueIdentifier, source: controller, destination: appNavigationController)
    
    controller.prepareForSegue(appNavSegue, sender: controller)
    controller.presentViewController(appNavigationController, animated: true, completion: nil)
    
  }
  
  internal func pushToAppNavigationController(controller : UIViewController){
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let appNavigationController = storyboard.instantiateViewControllerWithIdentifier("AppNavigationController") as AppNavigationController
    let appNavSegue = UIStoryboardSegue(identifier: "LoginSegue", source: controller, destination: appNavigationController)
    
    controller.prepareForSegue(appNavSegue, sender: controller)
    controller.presentViewController(appNavigationController, animated: true, completion: nil)
    
  }
  
  
  //******************* SEGUE PERFORMING  ******************
  //TODO: Implement this if needed, discard if not needed
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if(segue.identifier == "RegisterViewSegue") {
      let registrationViewController = segue.destinationViewController as RegisterViewController;
      
    }
  }
  
  @IBAction func unwindToViewController (sender : UIStoryboardSegue) {
    
    
  }
  
  
  
  
  
  
  
}

//******************* CONTROLLER LIFE CYCLE  ******************


extension LoginViewController {
  
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // load buttons
    loginButton.setBackgroundImage(UIAssets.imageOfLoginButton, forState: .Normal)
    loginButton.setBackgroundImage(UIAssets.imageOfLoginButtonPushed, forState: .Highlighted)
    
    registerButton.setBackgroundImage(UIAssets.imageOfRegisterButton, forState: .Normal)
    registerButton.setBackgroundImage(UIAssets.imageOfRegisterButtonPushed, forState: .Highlighted)
    
    //change background color
    self.view.backgroundColor = UIAssets.offWhite
    
    //set password to secure
    passwordTextBox.secureTextEntry = true
    
    
  }
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    loginButton.transform = CGAffineTransformMakeTranslation(-view.bounds.width, 0)
    registerButton.transform = CGAffineTransformMakeTranslation(-view.bounds.width, 0)
    
  }
  
  
  override func viewDidAppear(animated: Bool) {
    
    //start facebook button anim
    
    
    //UILoginAnimationBuilder.slideInFromLeft(loginButton, controllerOfView: self);
    
    UIView.animateWithDuration(0.4, delay: 0.0, options: .CurveEaseOut, animations: {
      self.loginButton.transform = CGAffineTransformIdentity
      }, completion: nil )
    
    UIView.animateWithDuration(0.6, delay: 0.0, options: .CurveEaseOut, animations: {
      self.registerButton.transform = CGAffineTransformIdentity
      }, completion: nil )
    
    
    
    
    
  }
  
  override func viewWillDisappear(animated: Bool) {
    //TODO: Check if I need this line
    //loginButton.center.x = self.view.frame.minX
    //stop facebook button anims timer.
    
  }
  
  
}

//******************* TEXTBOX DELEGATES  ******************


extension LoginViewController : UITextFieldDelegate {
  
  override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
    usernameTextBox.resignFirstResponder()
    passwordTextBox.resignFirstResponder()
  }
  //delegate the text input fields to close the keyboard
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    usernameTextBox.resignFirstResponder()
    passwordTextBox.resignFirstResponder()
    return true
  }
  
}


