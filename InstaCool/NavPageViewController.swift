//
//  NavPageViewController.swift
//  InstaCool
//
//  Created by Andrew Lincoln on 1/8/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit


protocol NavPageScrollToViewControllerDelegate {
  func moveToViewController(#navPageViewController : NavPageViewController);
}

var scrollEnabled = true;

class NavPageViewController: UIPageViewController {
  var isScrollEnabled : Bool!
  var homeViewController : HomeViewController!
  var scrollView : UIScrollView?
  var pageControllerDelegate : NavPageScrollToViewControllerDelegate?
  var viewControllersForLayout : [UIViewController]!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    //self.automaticallyAdjustsScrollViewInsets = false;
    self.navigationController?.navigationBarHidden = true;
    isScrollEnabled = true;
    scrollView = findScrollView();
    
    pageControllerDelegate?.moveToViewController(navPageViewController: self);
    
    self.view.multipleTouchEnabled = false;
    self.dataSource = self;
    //This prevents the scrollInsets to alter any autolayout constraints set on instantiated views
    self.view.insertSubview(UIView(), atIndex: 0);
    // prevents interesting layouts when this controller instantiates it's view controllers.
    viewControllersForLayout = getViewControllers()
   
    createPageViewControllers();
    
    homeViewController.transitionsDelegate = self;
    pageControllerDelegate = homeViewController;
  }
  
  override func viewWillAppear(animated: Bool) {
    println("view Appeared!")
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
}

extension NavPageViewController {
  func createPageViewControllers() {
    let initialViewController = viewControllersForLayout[1]
    let viewControllerArray : NSArray = [initialViewController];
    
    setViewControllers(viewControllerArray, direction: UIPageViewControllerNavigationDirection.Forward, animated: true, completion: nil);
  }
  
  func findScrollView() -> UIScrollView {
    for view in self.view.subviews {
      if view is UIScrollView {
        let scrollView = view as UIScrollView
        return scrollView
      } else {
        println("not setting")
      }
    }
    println("Couldn't find the scrollview")
    return  UIScrollView()
  }
  
  func getViewControllers() -> [UIViewController]  {
    let storyViewController = self.storyboard?.instantiateViewControllerWithIdentifier("StoriesViewController") as StoriesViewController;
    storyViewController.viewDidLoad()
    let friendsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("FriendsViewController") as FriendsViewController;
    homeViewController = self.storyboard?.instantiateViewControllerWithIdentifier("HomeController") as HomeViewController;
    let viewControllers : [UIViewController] = [storyViewController, homeViewController, friendsViewController];
    
    return viewControllers;
  }
}


extension NavPageViewController : UIPageViewControllerDataSource {
  func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
    if viewController is FriendsViewController {
      return viewControllersForLayout[1]
    } else if viewController is StoriesViewController {
      return nil
    }
    // if on home controller
    return viewControllersForLayout[0]
  }
  
  func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
    // nothing after friendsViewController
    if(viewController is FriendsViewController){
      return nil
    } else if(viewController is StoriesViewController){
      return viewControllersForLayout[1]
    }
    
    return viewControllersForLayout[2];
  }
  
}

extension NavPageViewController : UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    weAreRecording ? (scrollView.scrollEnabled = false) : (scrollView.scrollEnabled = true);
  }
}

extension NavPageViewController : TransitionsDelegate {
  func storiesButtonPushed() {
    //scrollView?.setContentOffset(self.view.bounds.origin, animated: true)
    var initialViewController = viewControllersForLayout[0]
    let viewControllerArray : NSArray = [initialViewController];
    // scrollView?.setContentOffset(CGPoint(x: ,y: 0), animated: true);
    setViewControllers(viewControllerArray, direction: UIPageViewControllerNavigationDirection.Reverse, animated: true, completion:nil);
  }
  
  func friendsButtonPushed() {
    var initialViewController = viewControllersForLayout[2]
    let viewControllerArray : NSArray = [initialViewController];
    setViewControllers(viewControllerArray, direction: UIPageViewControllerNavigationDirection.Forward, animated: true, completion:nil);
  }
  
  func recordButtonIsActive() {
    scrollView?.scrollEnabled = false;
  }
  
  func recordButtonIsInactive() {
    scrollView?.scrollEnabled = true;
  }
}


