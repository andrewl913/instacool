//
//  PreviewVideoViewController.swift
//  InstaCool
//
//  Created by Andrew Lincoln on 12/28/14.
//  Copyright (c) 2014 Andrew Lincoln. All rights reserved.
//

import UIKit
import AVFoundation



class PreviewVideoViewController: UIViewController {
  @IBOutlet weak var discardClipButton: UIButton!
  @IBOutlet weak var saveClipButton: UIButton!
  //UIImages
  let saveClipImage = UIAssets.imageOfSaveVideoButton(rotationAngle: 360)
  var savingClipAnimationImage : UIImage?
  let discardClipImage = UIAssets.imageOfDiscardChangesButton
  let saveClipFrames = 360;
  var arrClipImages : NSMutableArray!
  //var videoPlayer : AVPlayer?;
  var videoPreviewLayer : AVPlayerLayer?;
  var videoPlayer = AVPlayer();
  var videoClipURL : NSURL?
  let currentUser = PFUser.currentUser();
  var pfFile : PFFile?;
  var data : NSData?;
  //var videoPlayer : AVPlayer?;
}

extension PreviewVideoViewController {
  override func viewDidLoad() {
    super.viewDidLoad()
    makeSaveAnimImage();
    discardClipButton.setBackgroundImage(discardClipImage, forState: UIControlState.Normal)
    saveClipButton.setBackgroundImage(saveClipImage, forState: .Normal)
    saveClipButton.setBackgroundImage(saveClipImage, forState: .Highlighted);
  }

  override func viewWillAppear(animated: Bool) {
    saveClipButton.transform = CGAffineTransformMakeTranslation(-self.view.bounds.width, 0);
    discardClipButton.transform = CGAffineTransformMakeTranslation(self.view.bounds.width, 0);
  }
  
  override func viewDidAppear(animated: Bool) {
    UIAnimations.slideBackIn(saveClipButton, superView: self.view, delay: 0)
    UIAnimations.slideBackIn(discardClipButton, superView: self.view, delay: 0);
  }
  
  override func viewWillDisappear(animated: Bool) {
    videoPlayer.pause();
  }
}

  
extension PreviewVideoViewController : VideoClipPlayerDelegate {
  
  func makeSaveAnimImage(){
    arrClipImages = NSMutableArray(capacity: saveClipFrames);
    
    for(var i = saveClipFrames ; i > 0 ; i--) {
      arrClipImages.addObject(UIAssets.imageOfSaveVideoButton(rotationAngle: CGFloat(i)))
    }
    savingClipAnimationImage = UIImage.animatedImageWithImages(arrClipImages, duration: NSTimeInterval(2.5));
  }
  
  func playVideoClip(videoClip: NSURL) {
    var avAsset: AVAsset = AVAsset.assetWithURL(videoClip) as AVAsset;
    var avPlayerItem = AVPlayerItem(asset: avAsset);
    var error = videoPlayer.error;
    
    videoPlayer = AVPlayer(playerItem: avPlayerItem);
    videoPlayer.seekToTime(kCMTimeZero);
    videoClipURL = videoClip
    
    println("Video Players Current Item: \(videoPlayer.currentItem.duration.value.description)")

    self.videoPreviewLayer = AVPlayerLayer(player: videoPlayer);
    self.videoPreviewLayer?.frame = self.view.frame;
    self.videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill;
    self.view.layer.insertSublayer(videoPreviewLayer, atIndex: 0);
 
    videoPlayer.play();
    videoPlayer.actionAtItemEnd = AVPlayerActionAtItemEnd.None;
    
    NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("playerDidReachEnd:"), name: AVPlayerItemDidPlayToEndTimeNotification, object: videoPlayer.currentItem);
  }
  
  @IBAction func saveClipButton(sender: UIButton) {
    data = NSData(contentsOfURL: videoClipURL!);
    pfFile = PFFile(name: "clip.mov", data: data)!;
    
    pfFile!.saveInBackgroundWithBlock({
      (success : Bool, error : NSError!) in
      if(success) {
        println("File Saved!");
        
        var userVideoClip = PFObject(className: "UserVideoClip");
        userVideoClip["Clip"] = self.pfFile;
        userVideoClip["User"] = self.currentUser;
        userVideoClip["Username"] = self.currentUser.username;
        userVideoClip.saveInBackgroundWithBlock(){ success, error in
          success ? println("Saved to Parse") : println("Didnt save \(error.localizedFailureReason)");
          self.dismissViewControllerAnimated(true , completion: nil);
        }
      }
      }, progressBlock: {
        (percentFinished : CInt) in
        UIAnimations.smallBounce(self.saveClipButton, superView: self.view);
        println(percentFinished)
        self.saveClipButton.setBackgroundImage(self.savingClipAnimationImage, forState: UIControlState.Normal);
        self.saveClipButton.setBackgroundImage(self.savingClipAnimationImage, forState: UIControlState.Highlighted)
    });
  }
  
  @IBAction func deleteClipButton(sender: UIButton) {
    self.dismissViewControllerAnimated(true , completion: nil);
  }

  func playerDidReachEnd(notification : NSNotification) {
    let avPlayerItem = notification.object as? AVPlayerItem;
    avPlayerItem?.seekToTime(kCMTimeZero);
  } 
}