//
//  RegisterViewController.swift
//  InstaCool
//
//  Created by Andrew Lincoln on 12/21/14.
//  Copyright (c) 2014 Andrew Lincoln. All rights reserved.
//

import Foundation



class RegisterViewController: UIViewController , UITextFieldDelegate {
  @IBOutlet weak private var newUserName: UITextField!
  @IBOutlet weak private var newPassword: UITextField!
  @IBOutlet weak private var userEmail: UITextField!
  @IBOutlet weak private var registerButton: UIButton!
  @IBOutlet weak private var passwordStatusImage: UIImageView!
  @IBOutlet weak private var emailStatusImage : UIImageView!
  @IBOutlet weak private var backButton: UIButton!
  @IBOutlet weak private var registerTitle: UIImageView!
  
  let warningImage = UIAssets.imageOfWarningButton,
      checkMarkImage = UIAssets.imageOfCheckMarkButton,
      confirmButtonImage = UIAssets.imageOfConfirmButton,
      confirmButtonPushedImage = UIAssets.imageOfConfirmButtonPushed,
      backButtonImage = UIAssets.imageOfBackButton,
      backButtonImagePushed = UIAssets.imageOfGoBackPushed,
      registrationTitle = UIAssets.imageOfRegisterHeader;
  
  var alreadyValid = false;
  
  override func viewDidLoad() {
    super.viewDidLoad();
    
    self.view.backgroundColor = UIAssets.offWhite;
    //setup Registration Title
    registerTitle.image = registrationTitle;
    //encrypt password
    newPassword.secureTextEntry = true
  
    //setup of button images
    registerButton.setBackgroundImage(confirmButtonImage, forState: .Normal);
    registerButton.setBackgroundImage(confirmButtonPushedImage, forState: UIControlState.Highlighted);
    backButton.setBackgroundImage(backButtonImage, forState: UIControlState.Normal);
    backButton.setBackgroundImage(backButtonImagePushed, forState: UIControlState.Highlighted);
  }
  
  @IBAction func confirmRegistration(sender: AnyObject) {
    registerUser();
  }
  
  //gives the user a warning when the email field is not valid
  @IBAction func textFieldChangedEmail(sender: AnyObject) {
    
    if(!isValidEmail(userEmail.text)){
      alreadyValid = false;
      animateStatusImage(warningImage, image: emailStatusImage, animate: false);
      
    } else {
      //prevent the animation from firing off after valid email is inputted
      
      if(!alreadyValid){
        animateStatusImage(checkMarkImage, image : emailStatusImage, animate: true);
      }
      alreadyValid = true;
      animateStatusImage(checkMarkImage, image : emailStatusImage, animate: false);
    }
    
  } // end textFieldChangedEmail
  
  @IBAction func textFieldChangedPassword(sender: AnyObject) {
    //when password is not 8 characters
    println("Text Changed")
    
    //isValidTextInput(isValidPassword(newPassword.text), stopAnimVariable: alreadyValid, image: passwordStatusImage);
    
    if(!isValidPassword(newPassword.text)){
      alreadyValid = false;
      animateStatusImage(warningImage, image: passwordStatusImage, animate: false);
      
    } else {
      //prevent the animation from firing off after valid email is inputted
      if(!alreadyValid){
        animateStatusImage(checkMarkImage, image : passwordStatusImage, animate: true);
      }
      alreadyValid = true;
      animateStatusImage(checkMarkImage, image : passwordStatusImage, animate: false);
    }
    
  }// end textFieldChangedPassord
  
  @IBAction func passwordEditDidBegin(sender: AnyObject) {
    if(!isValidPassword(newPassword.text)){
      animateStatusImage(warningImage, image: passwordStatusImage, animate: true);
      
    }
  }
  // Doesn't Fade Out .. Just Disappears TODO: Make Fadeout.
  @IBAction func passwordEditDidEnd(sender: AnyObject) {
    if(!isValidPassword(newPassword.text)){
      animateStatusImageFadeOut(warningImage, image: passwordStatusImage, animate: true);
      
    }
    
  }
  
  
  @IBAction func emailEditDidBegin(sender: AnyObject) {
    println("email focus");
    if(!isValidEmail(userEmail.text)){
      animateStatusImage(warningImage, image: emailStatusImage, animate: true);
      
    }
  }
  
  @IBAction func emailEditDidEnd(sender: AnyObject) {
    if(!isValidEmail(userEmail.text)) {
      animateStatusImageFadeOut(warningImage, image: emailStatusImage, animate: true);
      
    }
  }
  
  //hitting the return key on the keyboard closes the keyboard.
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    newUserName.resignFirstResponder();
    newPassword.resignFirstResponder();
    userEmail.resignFirstResponder();
    return true;
    
  }
  
  // touching outside the keyboard will close the keyboard
  override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
    newUserName.resignFirstResponder();
    newPassword.resignFirstResponder();
    userEmail.resignFirstResponder();
  }
  
  //Custom Methods ---------------------------------
  
  func isValidPassword(testStr: String) -> Bool {
    println("Validating password");
    let passwordRegEx = "((?=.*[^a-zA-Z])(?=.*[a-z])(?=.*[A-Z]).{8,})";
    var passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegEx);
    let result = passwordTest?.evaluateWithObject(testStr);
    return result!;
  }
  
  func isValidEmail(testStr:String) -> Bool {
    println("validating email: \(testStr)")
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    
    var emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx);
    let result = emailTest?.evaluateWithObject(testStr);
    return result!;
  }
  
  func animateStatusImage( imageAsset : UIImage, image : UIImageView , animate: Bool) {
    
    image.image = imageAsset;
    if(animate == true){
      image.transform = CGAffineTransformMakeScale(0, 0);
      UIView.animateWithDuration(0.3, animations:{
        image.transform = CGAffineTransformMakeScale(1, 1);
        
      })
    } else {
      image.transform = CGAffineTransformMakeScale(1, 1);
    }
    
  }
  
  func animateStatusImageFadeOut(imageAsset: UIImage, image : UIImageView , animate: Bool) {
    image.image = imageAsset;
    if(animate == true){
      image.transform = CGAffineTransformMakeScale(1, 1);
      UIView.animateWithDuration(0.3, animations:{
        image.transform = CGAffineTransformMakeScale(0, 0);
        
      })
    } else {
      image.transform = CGAffineTransformMakeScale(0, 0);
    }
    
  }
  
  func registerUser() {
    var user = PFUser();
    
    if(newUserName.text == "") {
      newUserName.backgroundColor = UIAssets.errorRed;
      newUserName.placeholder = "Please Provide Username!";
    } else {
      user.username = newUserName.text;
    }
    
    if(newPassword.text == "") {
      newPassword.placeholder = "Please Enter a Password!";
    }
    
    if (!isValidPassword(newPassword.text)) {
      user.password = newPassword.text;
      newPassword.text = "";
      newPassword.placeholder = "Must be 8 characters!";
    }
    
    if(!isValidEmail(userEmail.text)) {
      userEmail.text = "";
      userEmail.placeholder = "Invalid Email address";
    }
    
    if(isValidEmail(userEmail.text) && isValidPassword(newPassword.text) && newUserName.text != "") {
      user.password = newPassword.text;
      user.email = userEmail.text;
      user.signUpInBackgroundWithBlock {(succeeded: Bool!, error : NSError!) -> Void in
        if(error == nil) {
          println("Successful Sign Up");
          //class method.
          LoginViewController.pushToAppNavigationController(self, segueIdentifier: "NewRegSegue");
        } else {
          println("There was an error registering");
        }
      }
    }
  }
  
  // This method doesn't function as intended so will keep this active for reference only!
  func isValidTextInput(inputText : Bool, var stopAnimVariable : Bool , image : UIImageView){
    
    if(!inputText){
      stopAnimVariable = false;
      animateStatusImage(warningImage, image: image, animate: false);
    } else {
      //prevent the animation from firing off after valid email is inputted
      if(!stopAnimVariable){
        animateStatusImage(checkMarkImage, image : image, animate: true);
      }
      stopAnimVariable = true;
      animateStatusImage(checkMarkImage, image : image, animate: false);
      //when it is 8 characters
      //remove warning
      //show success
    }
  }
}