//
//  DismissModalViewSegue.swift
//  InstaCool
//
//  Created by Andrew Lincoln on 1/1/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class DismissModalViewSegue : UIStoryboardSegue {
  
  override func perform() {
    var source = sourceViewController as UIViewController;
    var destination = destinationViewController as HomeViewController;
    //
    source.dismissViewControllerAnimated(false, completion: nil);
  }
}
