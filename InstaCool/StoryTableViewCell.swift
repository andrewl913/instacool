//
//  StoryTableViewCell.swift
//  InstaCool
//
//  Created by Andrew Lincoln on 1/14/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class StoryTableViewCell: UITableViewCell {
  @IBOutlet weak var usernameText: UILabel!
  
  
  @IBOutlet weak var displayStoryButton: UIButton!
  @IBAction func displayStoryButton(sender: UIButton) {
    
    
    
  }
  init(style: UITableViewCellStyle, reuseIdentifier: String?, storyIconImage : UIImage) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    displayStoryButton.setBackgroundImage(storyIconImage, forState: .Normal)
    displayStoryButton.setTitle("", forState: .Normal);
  }


  
  
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    // Configure the view for the selected state
  }
  
  func setStoryBoardButton (storyIconImage : UIImage) {
     displayStoryButton.setBackgroundImage(storyIconImage, forState: .Normal)
    displayStoryButton.setTitle("", forState: UIControlState.Normal)
  }
}
