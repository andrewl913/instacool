//
//  TransitionManager.swift
//  InstaCool
//
//  Created by Andrew Lincoln on 12/30/14.
//  Copyright (c) 2014 Andrew Lincoln. All rights reserved.
//

import UIKit

class TransitionManager:  UIPercentDrivenInteractiveTransition , UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning, UIViewControllerInteractiveTransitioning {
    
    var presenting = false;
    var interactive = false;
    private var enterPanGesture : UIScreenEdgePanGestureRecognizer!;
    
    var sourceViewController: UIViewController! {
        didSet {
            self.enterPanGesture = UIScreenEdgePanGestureRecognizer()
            self.enterPanGesture.addTarget(self, action:"handleOnstagePan:")
            self.enterPanGesture.edges = UIRectEdge.Right
            self.sourceViewController.view.addGestureRecognizer(self.enterPanGesture)
        }
    }
    
    // TODO: We need to complete this method to do something useful
    func handleOnstagePan(pan: UIPanGestureRecognizer){
        // how much distance have we panned in reference to the parent view?
        let translation = pan.translationInView(pan.view!)
        
        // do some math to translate this to a percentage based value
        let d = translation.x / CGRectGetWidth(pan.view!.bounds) * 0.5
        
        // now lets deal with different states that the gesture recognizer sends
        switch (pan.state) {
            
        case UIGestureRecognizerState.Began:
            // set our interactive flag to true
            self.interactive = true
            
            // trigger the start of the transition
            self.sourceViewController.performSegueWithIdentifier("RegisterViewSegue", sender: self);
            break
            
        case UIGestureRecognizerState.Changed:
            
            // update progress of the transition
            self.updateInteractiveTransition(d)
            break
            
        default: // .Ended, .Cancelled, .Failed ...
            
            // return flag to false and finish the transition
            self.interactive = false
            if(d < -0.2) {
                // threshold crossed: finish
                self.finishInteractiveTransition()
            }
            else {
                // threshold not met: cancel
                self.cancelInteractiveTransition()
            }
        }
    }
/// insert animation here.
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
       
        let container = transitionContext.containerView();
        let fromView = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!;
        let toView = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!;
        let offScreenLeft = CGAffineTransformMakeTranslation(-container.frame.width, 0);
        let offScreenRight = CGAffineTransformMakeTranslation(container.frame.width, 0 );
        
        
        UIView.animateWithDuration(NSTimeInterval(0), delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.0, options: nil, animations: {
            
            
            
            
            
            }, completion: { done in
                
        
        })
        
    }
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
        return 1;
    }
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return self;
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return self;
    }
    func interactionControllerForPresentation(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return self.interactive ? self : nil;
    }
    func interactionControllerForDismissal(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return self.interactive ? self : nil;
    }
    
   
    
}
