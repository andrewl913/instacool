//
//  AppNavigationController.swift
//  InstaCool
//
//  Created by Andrew Lincoln on 12/22/14.
//  Copyright (c) 2014 Andrew Lincoln. All rights reserved.
//

import Foundation

class AppNavigationController : UINavigationController  {
    override func viewDidLoad() {
        // Set the navigationbar to be transparent
        self.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default);
        self.navigationBar.shadowImage = UIImage();
        self.navigationBar.translucent = true;
        self.view.backgroundColor = UIColor.clearColor();
        self.navigationBar.backgroundColor = UIColor.clearColor();
        self.automaticallyAdjustsScrollViewInsets = false;
        self.toolbar.backgroundColor = UIColor.clearColor();
        self.toolbar.translucent = true;
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning();
    }  
}

