//
//  ViewController.swift
//  InstaCool
//
//  Created by Andrew Lincoln on 12/20/14.
//  Copyright (c) 2014 Andrew Lincoln. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
   @IBOutlet weak var pickedImage: UIImageView!
    var activityIndicator = UIActivityIndicatorView();
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        println("image selected");
        self.dismissViewControllerAnimated(true, completion: nil);
        pickedImage.image = image;
    }
    
    
    @IBAction func pauseApp(sender: AnyObject) {
        activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50));
        activityIndicator.center = self.view.center;
        activityIndicator.hidesWhenStopped = true;
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray;
        view.addSubview(activityIndicator);
        activityIndicator.startAnimating();
        //UIApplication.sharedApplication().beginIgnoringInteractionEvents();
        
    }
    
    
    @IBAction func createAlert(sender: AnyObject) {
        var alert = UIAlertController(title: "Hey There!", message: "Are you surre", preferredStyle: UIAlertControllerStyle.Alert);
        self.presentViewController(alert, animated: true, completion: nil);
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { action in
            alert.dismissViewControllerAnimated(true, completion: nil);
        }))
        
        
    }
    
    
    @IBAction func restoreApp(sender: AnyObject) {
        activityIndicator.stopAnimating();
        //UIApplication.sharedApplication().endIgnoringInteractionEvents();
        
    }
    
    @IBAction func pickImage(sender: AnyObject) {
        var image : UIImagePickerController = UIImagePickerController();
        image.delegate = self;
        image.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
        image.allowsEditing = false;
        self.presentViewController(image, animated: true, completion: nil);
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

