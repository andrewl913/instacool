//
//  StoryViewController.swift
//  InstaCool
//
//  Created by Andrew Lincoln on 12/30/14.
//  Copyright (c) 2014 Andrew Lincoln. All rights reserved.
//

import UIKit

class StoriesViewController: UITableViewController {
  var userStories : [NSArray]?
  var query : PFQuery!
  var usernames : [NSString] = [];
  var clipsArray = [];
  var pfRelation : PFRelation?
  var videoClips : [PFObject]?
  var refreshView : RefreshView?
  var customRefreshControl : UIRefreshControl?
  
  //let refreshViewHeight : CGFloat = 200;
  override func viewDidLoad() {
    super.viewDidLoad();
    println("view loaded")
    self.tableView.separatorColor = UIAssets.boulderGray
  
    setUpRefreshControl()

  }
  
  override func viewWillAppear(animated: Bool) {
    println("Stories View Appeared")
    getUsersStories()
  }
}

extension StoriesViewController {
  func setUpRefreshControl() {
    customRefreshControl = UIRefreshControl()
    customRefreshControl?.backgroundColor = UIAssets.successGreen
    self.refreshControl = customRefreshControl;
    customRefreshControl?.addTarget(self, action: "refresh", forControlEvents: UIControlEvents.ValueChanged)
  }

  func refresh() {
    println("Refreshing")
    customRefreshControl?.beginRefreshing()
    self.tableView.reloadData();
    customRefreshControl?.endRefreshing()
    
    
  }
}

extension StoriesViewController {
  func getUsersStories() {
    query = PFQuery(className: "UserVideoClip")
    query.selectKeys(["Username"])
    query.findObjectsInBackgroundWithBlock() { clips , error in
      if (error == nil ) {
        self.usernames.removeAll(keepCapacity: true);
        for clip in clips {
          if let userClip = clip.valueForKey("Username") as? NSString {
            self.usernames.append(userClip)
          }
          self.tableView.reloadData();
        }
      }
    }
  }
}

extension StoriesViewController {
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let storyCell  = tableView.dequeueReusableCellWithIdentifier("StoryCell") as StoryTableViewCell
    
    storyCell.usernameText.text = usernames[indexPath.row]
    storyCell.setStoryBoardButton(UIAssets.imageOfCheckMarkButton)
    
    
    return storyCell;
  }
  
  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return "Hello"
  }
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return usernames.count
  }
  
  }
//************* DELEGATE METHODS ***********************
extension StoriesViewController {
  override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let headerView = UIView(frame: CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.bounds.width  ,self.view.bounds.height/6))
    let headerLabel = UILabel(frame: CGRectMake(headerView.bounds.width/2 - 60, self.view.bounds.width/12 - 10 , 120, 40))
    
    headerLabel.text = "Stories"
    headerLabel.textAlignment = NSTextAlignment.Center
    headerLabel.textColor = UIAssets.offWhite
    headerLabel.font = UIFont(name: "GillSans-LightItalic", size: 40)
    
    headerView.addSubview(headerLabel);
    headerView.backgroundColor = UIAssets.successGreen
    
    return headerView;
  }
  
  override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 90
  }

}