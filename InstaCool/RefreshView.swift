//
//  RefreshView.swift
//  InstaCool
//
//  Created by Andrew Lincoln on 1/14/15.
//  Copyright (c) 2015 Andrew Lincoln. All rights reserved.
//

import UIKit

class RefreshView: UIView {
  private unowned var scrollView : UIScrollView
  var progressPercentage : CGFloat = 0;
  
  class var refreshViewVisibleHeight : CGFloat {
    get {
      return 200
    }
  }
  
  class var sceneHeight : CGFloat {
    get {
      return 120
    }
  }

  
  override init () {
    scrollView = UIScrollView()
    super.init()
  }
  
  required init(coder aDecoder: NSCoder) {
    scrollView = UIScrollView()
    assert(false, "Use another initializer")
    super.init(coder: aDecoder)
  }
  
  init(frame: CGRect, scrollView : UIScrollView, refreshColor : UIColor) {

    self.scrollView = scrollView
    super.init(frame: frame)
    self.backgroundColor = refreshColor
  }
}

extension RefreshView : UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    let refreshViewVisibleHeight = max(0, -(scrollView.contentOffset.y + scrollView.contentInset.top) )
    progressPercentage = min(1, refreshViewVisibleHeight / RefreshView.sceneHeight)
    NSLog("Refresh view visible height = \(progressPercentage)")
    if(refreshViewVisibleHeight > 100) {
      
    }
  }
}

