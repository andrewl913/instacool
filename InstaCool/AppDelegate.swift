//
//  AppDelegate.swift
//  InstaCool
//
//  Created by Andrew Lincoln on 12/20/14.
//  Copyright (c) 2014 Andrew Lincoln. All rights reserved.
//

import UIKit
import Foundation




@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
      
      self.window?.backgroundColor = UIAssets.boulderGray

      
      
        //parse id key must be set before application is launched
        Parse.setApplicationId("", clientKey: "");
        PFFacebookUtils.initializeFacebook();
        //DEBUG: log the user out
        //PFUser.logOut();
        
        let isRegistered = PFUser.currentUser();
        let facebookSession = PFFacebookUtils.session();
        
        // Show homecontroller upon successful log in
        if(isRegistered != nil || facebookSession != nil){
            let permissions = ["public_profile"];
            let storyboard = UIStoryboard(name: "Main", bundle: nil);
            let homeController = storyboard.instantiateViewControllerWithIdentifier("AppNavigationController") as AppNavigationController;
            var storyViewController = storyboard.instantiateViewControllerWithIdentifier("StoriesViewController") as StoriesViewController
          
            storyViewController.getUsersStories();
          
            storyViewController.removeFromParentViewController()
            self.window?.rootViewController = homeController;
            
            self.window?.makeKeyAndVisible();
            //DEBUG:
             println("Registered UserName: \(isRegistered.username) email: \(isRegistered.email)");
    
        } else {
            //DEBUG:
            println("User is not logged in");
        }// end if

        return true
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool {
        
        return FBAppCall.handleOpenURL(url, sourceApplication: sourceApplication, withSession: PFFacebookUtils.session()); 
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        //wake up facebook account
        FBAppCall.handleDidBecomeActiveWithSession(PFFacebookUtils.session());
        
        
        
        
        
        
        
    }
    
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}

