//
//  PushNoAnimationSegue.swift
//  InstaCool
//
//  Created by Andrew Lincoln on 12/29/14.
//  Copyright (c) 2014 Andrew Lincoln. All rights reserved.
//

import UIKit

class PushNoAnimationSegue: UIStoryboardSegue {
    
    override func perform() {
        let source = sourceViewController as UIViewController
        if let navigation = source.navigationController {
            navigation.pushViewController(destinationViewController as UIViewController, animated: false)
          
          //
        }
    }
   
}
