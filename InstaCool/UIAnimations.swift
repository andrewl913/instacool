//
//  UIAnimationBuilder.swift
//  InstaCool
//
//  Created by Andrew Lincoln on 12/30/14.
//  Copyright (c) 2014 Andrew Lincoln. All rights reserved.
//

import Foundation
import UIKit

class UIAnimations  {
  
  //******************* CLASS FUNCTIONS ******************
  
  class func slideOutLeft(viewToAnimate : UIView, superView : UIView) {
    let offScreenLeft = CGAffineTransformMakeTranslation(superView.frame.width, 0);
    
    UIView.animateWithDuration(NSTimeInterval(1)) {
      viewToAnimate.transform = offScreenLeft;
    }
  }
  
  class func slideOutTop(viewToAnimate : UIView, superView : UIView) {
    let offScreenTop = CGAffineTransformMakeTranslation(0, -superView.frame.height);
    
    UIView.animateWithDuration(NSTimeInterval(1)) {
      viewToAnimate.transform = offScreenTop;
    }
  }
  
  class func slideOutRight(viewToAnimate : UIView, superView : UIView) {
    let offScreenRight = CGAffineTransformMakeTranslation(-superView.frame.width, 0);
    
    UIView.animateWithDuration(NSTimeInterval(1)) {
      viewToAnimate.transform = offScreenRight;
    }
  }
  
  class func slideBackIn(viewToAnimate : UIView, superView : UIView , delay : NSTimeInterval) {
    UIView.animateWithDuration(0.5, delay: delay, options: nil, animations: {
      viewToAnimate.transform = CGAffineTransformIdentity;
      }, completion: nil)
  }
  
  class func scaleOut(viewToAnimate : UIView, superView : UIView) {
    UIView.animateWithDuration(NSTimeInterval(0.5)) {
      viewToAnimate.transform = CGAffineTransformMakeScale(0.0001, 0.0001);
    }
  }
  
  class func scaleIn(viewToAnimate : UIView, superView : UIView) {
    UIView.animateWithDuration(NSTimeInterval(0.5)) {
      viewToAnimate.transform = CGAffineTransformIdentity;
    }
  }
  
  class func smallBounce(viewToAnimate : UIView, superView : UIView) {
    UIView.animateWithDuration(4.0, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 1, options: UIViewAnimationOptions.CurveEaseIn, animations: {
      viewToAnimate.transform = CGAffineTransformMakeScale(1.1, 1.1)
      }, completion: {
        done in
    })
  }
  
  class func makeIdentity(viewToAnimate : UIView, superView : UIView) {
    UIView.animateWithDuration(NSTimeInterval(0.5)) {
      viewToAnimate.transform = CGAffineTransformIdentity;
    }
  }
  
  //******************* INSTANCE METHODS ******************
  
  
  
  
  
  
}
