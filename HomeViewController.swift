//
//  HomeController.swift
//  SnapUp
//  Created by Andrew Lincoln on 12/22/14.
//  Copyright (c) 2014 Andrew Lincoln. All rights reserved.
//

import Foundation
import MediaPlayer
import UIKit
import MobileCoreServices
import Social
import MessageUI
import AVFoundation
import CoreMedia

protocol VideoClipPlayerDelegate {
  
  func playVideoClip(videoClip : NSURL);
}

protocol TransitionsDelegate {
  func storiesButtonPushed()
  func friendsButtonPushed()
  func recordButtonIsActive()
  func recordButtonIsInactive()
}

//This is the only global variable. Searching for an elegant method to notify the NavPageViewController when this changes.
var weAreRecording : Bool = false;

class HomeViewController: UIViewController, AVCaptureFileOutputRecordingDelegate , UIGestureRecognizerDelegate{
  //This subView is where the camera lives.
  @IBOutlet weak var welcomeBack: UILabel!
  @IBOutlet weak var cameraView: UIView!
  @IBOutlet weak var recordButton : UIButton!
  @IBOutlet weak var captureTime: UILabel!
  @IBOutlet weak var friendsButton: UIButton!
  @IBOutlet weak var storiesButton: UIButton!
  //user session variables
  var facebookSession : FBSession!;
  var userSession : PFUser!;
  var clipNumber : Int = 0;
  //camera setup variables and constants
  var captureSession : AVCaptureSession?;
  var backCamera : AVCaptureDevice?
  var microphone : AVCaptureDevice?;
  var inputDevice : AVCaptureDeviceInput?;
  var audioInputDevice : AVCaptureDeviceInput?;
  var videoOutput : AVCaptureVideoDataOutput?;
  var movieOutput : AVCaptureMovieFileOutput?;
  var previewLayer : AVCaptureVideoPreviewLayer?;
  //Timer
  var captureTimeInt : Int = 10;
  var captureTimer : NSTimer?;
  //OutputFile
  var outputURL : NSURL?;
  //recordButton Animation set up
  let recordButtonFrames = 360
  var arrRecordButton : NSMutableArray?;
  var recordAnimImage : UIImage?;
  let notRecordingImage : UIImage = UIAssets.imageOfRecordingButton(recordingAngle: 360);
  //NavPageViewController Instance
  var navPageViewController : NavPageViewController?;
  //transistions
  
  //App Icons
  let friendsButtonImage = UIAssets.imageOfFriendsIcon
  let friendsButtonImagePushed = UIAssets.imageOfFriendsIconPushed
  let storiesButtonImage = UIAssets.imageOfStoriesIcon;
  let storiesButtonImagePushed = UIAssets.imageOfStroriesIconPushed
  //Delegates
  var videoDelegate : VideoClipPlayerDelegate?;
  var transitionsDelegate : TransitionsDelegate?
  
  func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    return true;
  }
}
//******************* IBACTIONS  ******************
extension HomeViewController {
  @IBAction func storiesButton(sender: UIButton) {
    transitionsDelegate?.storiesButtonPushed()
  }
  
  @IBAction func friendsButton(sender: AnyObject) {
    transitionsDelegate?.friendsButtonPushed()
  }
  
  @IBAction func recordButton(sender: UIButton) {
    var outputPath : NSString = NSString(string: NSTemporaryDirectory() + "output.mov");
    var fileManager : NSFileManager = NSFileManager.defaultManager();
    println("Recording Video!");
    weAreRecording = true;
    transitionsDelegate?.recordButtonIsActive()
    outputURL = NSURL(fileURLWithPath: outputPath)!;
    
    if sender.touchInside { println("Inside")};
    
    startTimer();
    
    if(fileManager.fileExistsAtPath(outputPath)){
      var error : NSError?;
      
      if(fileManager.removeItemAtPath(outputPath, error: &error) == true){
        println("There was an error : \(error?.localizedFailureReason)");
      }
    }
    movieOutput?.startRecordingToOutputFileURL(outputURL, recordingDelegate: self);
    // movieOut
  }
}
//******************* VIEW CONTROLLER LIFE CYCLE ******************
extension HomeViewController {
  override func viewDidLoad() {
    super.viewDidLoad();
    // Remove navbar from view.. issue due to pushing to other nav controllers and returning back from them.
    self.navigationController?.navigationBarHidden = true;
    UIApplication.sharedApplication().statusBarHidden = true;
    //remove multitouch that could cause issues with the record button
    self.view.multipleTouchEnabled = false;
    //Initialize the animation for record button
    arrRecordButton = NSMutableArray(capacity: recordButtonFrames);
    
    for(var i = recordButtonFrames; i > 0 ; i--) {
      arrRecordButton?.addObject(UIAssets.imageOfRecordingButton(recordingAngle: CGFloat(i)));
    }
    
    recordAnimImage  = UIImage.animatedImageWithImages(arrRecordButton!, duration: 10);
    recordButton.setBackgroundImage(UIAssets.imageOfRecordingButton(recordingAngle: 360), forState: UIControlState.Normal);
    //load icon assets
    friendsButton.setBackgroundImage(friendsButtonImage, forState: UIControlState.Normal)
    friendsButton.setBackgroundImage(friendsButtonImagePushed, forState: UIControlState.Highlighted)
    storiesButton.setBackgroundImage(storiesButtonImage, forState: .Normal);
    storiesButton.setBackgroundImage(storiesButtonImagePushed, forState: UIControlState.Highlighted)
    //transition
    
    //navPageViewController
    
    welcomeBack.layer.zPosition = 1;
    facebookSession = PFFacebookUtils.session();
    userSession = PFUser.currentUser();
    
    // if the user is a facebook user, lets get their facebook information
    if(facebookSession != nil){
      var facebookReq = FBRequest(session: facebookSession, graphPath: "/me", parameters: nil, HTTPMethod: "GET");
      
      FBRequestConnection.startForMeWithCompletionHandler({
        (connection : FBRequestConnection! , result : AnyObject!, error : NSError!) in
        if(error == nil){
          println("User Info: \(result)");
        }
      })
    } else {
      // user is not a facebook user
      welcomeBack.text = userSession.username;
    }
    //Find input devices
    let devices = AVCaptureDevice.devices()
    
    for device in devices {
      if(device.hasMediaType(AVMediaTypeAudio)) { microphone = device as? AVCaptureDevice }
      
      if (device.hasMediaType(AVMediaTypeVideo)) {
        if(device.position == AVCaptureDevicePosition.Back) {
          backCamera = device as? AVCaptureDevice
        }
      }
    }
  }// end viewDidLoad
  
  override func viewWillAppear(animated: Bool) {
    println("view Appeared!");
    UIApplication.sharedApplication().statusBarHidden = true;
    // view is a about to appear, so we aren't recording.
    weAreRecording = false;
    
    getNavPageViewControllerInstance();
    
    beginSession();
    
    sessionPreset(AVCaptureSessionPresetMedium);
    
    //if buttons were scaled out from the transition animation, scale them back in
    UIAnimations.scaleIn(welcomeBack, superView: self.view);
    UIAnimations.scaleIn(friendsButton, superView: self.view);
    UIAnimations.scaleIn(recordButton, superView: self.view);
    //set up the delegate
  }
  
  override func viewDidDisappear(animated: Bool) {
    println("viewWilldisappear! : HOMEVIEWCONTROLLER")
    //captureSession?.stopRunning();
    //captureSession?.removeInput(inputDevice);
    //captureSession = nil;
    // previewLayer?.removeFromSuperlayer();
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if(segue.identifier == "PreviewSegue"){
      let previewController = segue.destinationViewController as PreviewVideoViewController;
      self.videoDelegate = previewController;
      self.videoDelegate?.playVideoClip(self.outputURL!);
      outputURL = nil;
    }
    
    if(segue.identifier == "FriendsViewSegue"){
      let friendsController = segue.destinationViewController as FriendsViewController;
    }
    
    if(segue.identifier == "StoriesViewSegue") {
      let storiesController = segue.destinationViewController as StoriesViewController;
    }
  }
  //************* PUSH VIDEO VIEW CONTROLLERS OFF CONTROLLER STACK ***
  @IBAction func unwindToViewController (sender : UIStoryboardSegue) {
    println("Unwinding home !!");
    scrollEnabled = true;
    // self.dismissViewControllerAnimated(true, completion: nil)
  }
}

//******************* RECORDING LOGIC ******************
extension HomeViewController {
  
  //Two ways to stop the recording, either the timer expires or user ends the recording denoted by releasing the record button.
  
  //******************* STOP RECORDING  ******************
  
  @IBAction func stopRecording(sender: AnyObject) {
    transitionsDelegate?.recordButtonIsInactive();
    weAreRecording = false;
    println("Recording Stopped");
    
    movieOutput?.stopRecording();
    
    self.view.userInteractionEnabled = true;
    //reset positions of UI Elements
    slideInButtons();
    
    recordButton.setBackgroundImage(notRecordingImage, forState: UIControlState.Normal);
    
    captureTimer?.invalidate();
    
    captureTimeInt = 10;
  }
  
  func slideInButtons() {
    UIAnimations.slideBackIn(friendsButton, superView: self.view, delay: 3);
    UIAnimations.slideBackIn(welcomeBack, superView: self.view , delay: 3);
    UIAnimations.slideBackIn(storiesButton, superView: self.view, delay: 3);
  }
  
  //******************* FINISHED RECORDING **************
  //this is a AVCapture delegate
  func captureOutput(captureOutput: AVCaptureFileOutput!, didFinishRecordingToOutputFileAtURL outputFileURL: NSURL!, fromConnections connections: [AnyObject]!, error: NSError!) {
    var recordedSuccessfully = true;
    var data : NSData?;
    
    if(error != nil) {
      println("Video didn't successfully record \(error.localizedDescription) Failed : \(error.localizedFailureReason)");
      recordedSuccessfully = false;
    }
    
    if(recordedSuccessfully) {
      self.performSegueWithIdentifier("PreviewSegue", sender: self);
      println("Recording Successful");
    }
  }
  //******************* VIDEO RESOLUTION ******************
  private func sessionPreset(preset : String!) {
    if(captureSession != nil){
      captureSession!.canSetSessionPreset(preset) ? captureSession?.sessionPreset = preset :
        println("Can't set session preset resolution/quality.");
    }
  }
  //******************* CAMERA INPUT SETTINGS ******************
  private func beginSession() {
    /* do to the nature of segues animated transitions, the viewWillAppear and viewDidAppear methods get called
    regardless of whether or not we complete or cancel the transaction, we must check the capture session is not existing.
    If not, the captureSession object will produce many instances of itself.
    */
    if(captureSession == nil){
      var err : NSError? = nil;
      captureSession = AVCaptureSession();
      
      inputDevice = AVCaptureDeviceInput(device: backCamera, error: &err);
      audioInputDevice = AVCaptureDeviceInput(device: microphone, error: &err);
      
      if(captureSession != nil ){
        if(captureSession!.canAddInput(audioInputDevice) && captureSession!.canAddInput(inputDevice)){
          captureSession?.addInput(audioInputDevice)
          captureSession?.addInput(inputDevice);
        }
      }
      
      if(err != nil) {
        println("error: \(err!.localizedDescription)");
      }
      // Add Video Output
      movieOutput = AVCaptureMovieFileOutput();
      
      var totalSeconds : Int64 = 10;
      var preferredTimeScale : Int32 = 30;
      var maxDuration : CMTime = CMTimeMake(totalSeconds, preferredTimeScale);
      var layerRect : CGRect = self.view.layer.bounds;
      
      movieOutput?.minFreeDiskSpaceLimit = 1024 * 1024;
      
      if(captureSession!.canAddOutput(movieOutput)){
        captureSession?.addOutput(movieOutput);
      }
      //******************* PREVIEW LAYER ******************
      previewLayer = AVCaptureVideoPreviewLayer(session: captureSession);
      previewLayer?.frame = CGRectMake(0, 0, self.view.layer.frame.width, self.view.layer.frame.height)
      previewLayer?.bounds = layerRect;
      previewLayer?.position = CGPointMake(CGRectGetMidX(layerRect), CGRectGetMidY(layerRect));
      previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill;
      self.view.layer.insertSublayer(previewLayer, atIndex: 0)
      //DEBUG:
      if(captureSession!.interrupted) {
        println("Session is interrupted")
      }
      captureSession?.commitConfiguration();
      
      self.captureSession!.startRunning();
    }
  }
  
  func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
    return weAreRecording ? false : true;
  }
  //******************* START VIDEO TIMER  ******************
  func startTimer() {
    captureTimer =  NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("startCaptureTime"), userInfo: nil, repeats: true);
  }
  
  func startCaptureTime(){
    var backGroundImage = recordButton.backgroundImageForState(.Normal);
    //slide all UIbuttons out
    UIAnimations.slideOutLeft(friendsButton, superView: self.view);
    UIAnimations.slideOutTop(welcomeBack, superView: self.view);
    UIAnimations.slideOutRight(storiesButton, superView: self.view)
    
    if(backGroundImage != recordAnimImage){
      recordButton.setBackgroundImage(recordAnimImage, forState: UIControlState.Normal);
    }
    
    println(captureTimeInt);
    //stop recording when capture time is greater than 10 seconds
    if(captureTimeInt <= 0) {
      weAreRecording = false;
      captureTimer?.invalidate();
      captureTimeInt = 10;
      movieOutput?.stopRecording();
      //commented to change UI
      recordButton.setBackgroundImage(notRecordingImage, forState: UIControlState.Normal)
      slideInButtons();
    } else {
      captureTimeInt % 2 == 0 ? startRecordingButtonAnim() : stopRecordingButtonAnim();
    }
    captureTimeInt--;
  }
  //******************* ANIMATE RECORDING BUTTON **************
  func startRecordingButtonAnim(){
    UIView.animateWithDuration(0.5) {
      self.recordButton.transform  = CGAffineTransformMakeScale(1.1, 1.1);
    }
  }
  
  func stopRecordingButtonAnim(){
    UIView.animateWithDuration(0.5) {
      self.recordButton.transform  = CGAffineTransformMakeScale(1, 1);
    }
  }
  
  func getNavPageViewControllerInstance() {
    let navViewController = self.parentViewController
    
    if navViewController is NavPageViewController {
      navPageViewController = navViewController as? NavPageViewController;
      println("found it!")
    }
  }
}

extension HomeViewController : NavPageScrollToViewControllerDelegate {
  func moveToViewController(#navPageViewController: NavPageViewController) {
  }
}






