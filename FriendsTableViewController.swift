//
//  FriendsTableViewController.swift
//  InstaCool
//
//  Created by Andrew Lincoln on 12/28/14.
//  Copyright (c) 2014 Andrew Lincoln. All rights reserved.
//

import UIKit

class FriendsTableViewController: UIViewController {

    @IBOutlet weak var backHomeButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func backHomeButton(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}


